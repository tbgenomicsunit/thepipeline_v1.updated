#! /bin/bash
# 
# This is an example script to run ThePipeline within a directory containing
# a whole dataset. It is necessary to change files extensions and program 
# parameters for a succesful run. E.G ls *.fastq.gz must be used with
# --compressed parameter in ThePipeline kraken and with --paired if there are
# paired end reads.
#
# Since this pipeline and scripts are ment to be used in koch and yersin
# ThePipeline kraken steps can only be run in koch, so remove those lines
# if runing in yersin or if contamination cleaning is not needed.
#
#
# IMPORTANT: This script is going to be replaced ASAP by a new, fancy and handy
# subcommand: ThePipeline pipeit

PROCS=10

# Now map filtered reads with bwa. We are goin to use
# just 2 bwa threads as we are going to spawn PROCS
# number of ThePipeline mapping instances

ls *.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} ThePipeline mapping -f {}\_1.filtered.fastq {}\_2.filtered.fastq --prefix {} -t 2

# Now perform variant calling also in parallel
ls *.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} ThePipeline coverage --prefix {}
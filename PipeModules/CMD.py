def build_CMD(config):
	'''read a .config file and build a CMD to run a program'''

	cmd = []
	with open(config) as infile:
		for line in infile:
			if line.startswith("#"):
				continue
			elif line.startswith("PROGRAM"):
				line = line.rstrip().split("=")
				program = line[1]
				cmd.append(program)
			else:
				line = line.rstrip().split()
				cmd.extend(line)
	return cmd
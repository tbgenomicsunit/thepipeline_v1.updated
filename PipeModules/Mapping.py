def ReadConfig(cfgfile):
    '''This function read paramters from a picard config file. If no config file
    is provided, then it reads from
    ThePipeline/PipeModules/PipeConfigs/picard_default.config where parameters
    are specified like:

    ASSUME_SORTED=true
    REMOVE_DUPLICATES=true
    VALIDATION_STRINGENCY=LENIENT

    it returns a list of parameters ready tu build the command line (CMD) to
    call the picard
    '''

    import os
    import sys

    parameters = []

    if cfgfile == "default_config":
        modules_dir = os.path.dirname(os.path.abspath(__file__))
        configs_dir = "{}/PipeConfigs".format(modules_dir)
        config = "{}/picard_default.config".format(configs_dir)
        infile = open(config)
    else:
        try:
            infile = open(cfgfile)
        except IOError:
            sys.exit("Configuration file not found")

    for line in infile:
        if not line.startswith("#"):
            parameter = line.rstrip()
            parameters.append(parameter)

    infile.close()

    return parameters

def bwa_map(fastq, reference, threads, index, prefix):
    ''' Use bwa to map inputf fastq/s to reference. It is always expected that
    all programs this pipeline depends on are stored under TP_programs and data
    as reference genomes are stored under TP_data.

    When no reference is provided, ./TP_data/references/MTB_ancestor.fasta is
    used by default.

    Reference is indexed thorugh bwa index if -i/--index option is used or
    directly used as reference otherwise

    Exit status of each command is stored in stat and pipeline stops if one
    of them fails

    stat == 0 means a command succeded
    stat != 0 means a command failed

    '''
    import subprocess as sp
    import sys
    from Repository import Programs, Data
    from History import UpdateHistory

    programs = Programs()
    data = Data()
    bwa = programs["bwa"]
    samtools = programs["samtools"]

    if not reference:
        reference = data["reference"]

    if index: # if -i/--index option provided index reference
        stat = sp.call([bwa, "index", "-a", "bwtsw", reference])
        # Check that command succeeded
        if stat != 0:
            sys.exit("Pipeline stopped at bwa index point!\n")

    # Read Group to pass to bwa to make it compatible with GATK
    rgroup = '@RG\\tID:' + prefix + '\\tSM:' + prefix
    rgroup += '_sm\\tPU:' + prefix + '_pu\\tLB:' + prefix + '_lb'

    cmd_bwa = [ bwa, "mem", "-t", threads, "-R", rgroup, reference ]
    # Add an awk line to filter alignments with MAPQ < 60
    cmd_awk = [ 'awk',  '$1 ~ /^@/ || $5 == 60' ]
    cmd_sam = [ samtools, "view", "-bt", reference, "-", "--threads", threads ]
    cmd_sort = [ samtools, "sort",  "-o", "{}.sort.bam".format(prefix),
                "--threads", threads ]

    for fq in fastq:
        cmd_bwa.append(fq)
    mem = sp.Popen(cmd_bwa, stdout=sp.PIPE)
    awk_MAPQ60 = sp.Popen(cmd_awk, stdin=mem.stdout, stdout=sp.PIPE)
    mem.stdout.close()
    view = sp.Popen(cmd_sam, stdin=awk_MAPQ60.stdout, stdout=sp.PIPE)
    awk_MAPQ60.stdout.close()
    sort = sp.Popen(cmd_sort, stdin=view.stdout)
    view.stdout.close()
    # awk_MAPQ60.stdout.close()
    # view.stdout.close()
    out, err = sort.communicate()

    UpdateHistory(cmd_bwa, "bwa", prefix)
    UpdateHistory(cmd_sam, "samtools", prefix)
    UpdateHistory(cmd_awk, "awk", prefix)
    UpdateHistory(cmd_sort, "samtools", prefix)

    return 0

def RemoveDuplicates(args):
    '''This function builds a CMD to mark and remove duplicates from .sort.bam
    files using picard. Default parameters are specified in
    ThePipeline/PipeModules/PipeConfigs/picard_default.config:

    ASSUME_SORTED=true
    REMOVE_DUPLICATES=true
    VALIDATION_STRINGENCY=LENIENT
    '''
    import subprocess as sp
    import sys, os
    from Repository import Programs, Data
    from History import UpdateHistory

    programs = Programs()
    data = Data()
    picard = programs["picard"]

    picard_parameters = ReadConfig(args.cfgfile)
    CMD = ["java", "-Xmx8g", "-jar", picard, "MarkDuplicates"]
    CMD.append("I={}.sort.bam".format(args.prefix))
    CMD.append("O={}.nd.sort.bam".format(args.prefix))
    CMD.append("M={}.dup.metrix".format(args.prefix))
    CMD.extend(picard_parameters)

    stat = sp.call(CMD)
    if stat != 0:
        sys.exit("Pipeline stopped at picard when removing duplicates!\n")
    UpdateHistory(CMD, "picard", args.prefix)
    # Now mv nd.sort.bam to .sort.bam extension so it is compatible with
    # subsequent analysis steps in the pipeline
    if not args.keepduplicatesbam:
        os.rename("{}.nd.sort.bam".format(args.prefix),
                  "{}.sort.bam".format(args.prefix))
    else:
        os.rename("{}.sort.bam".format(args.prefix),
                  "{}.dup.sort.bam".format(args.prefix))
        os.rename("{}.nd.sort.bam".format(args.prefix),
                  "{}.sort.bam".format(args.prefix))

    return 0

def Mapping(args):
    '''Call previous functions to perform a complete subpipeline'''
    import sys

    # Check than only one or two fastqs have been provided
    if len(args.fastq) != 2 and len(args.fastq) != 1:
        sys.exit("Up to two fastq files can be provided for mapping\n"
            "Not more. Not less\n")

    bwa_map(args.fastq, args.reference, args.threads, args.index, args.prefix)
    if args.nodedup == False:
        RemoveDuplicates(args)

    return 0

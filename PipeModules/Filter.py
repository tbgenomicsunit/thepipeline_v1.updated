def LoadAnnotation(anno_file):
    '''Load the annotation file in .ppt format
    to store each position with all its
    info in a dictionary

    This function is meant to load by default
    the H37Rv annotation under
    /data/ThePipeline/PipeModules/PipeScripts/H37Rv_annotation2sytems.ptt
    '''
    annotation = {}
    with open(anno_file) as infile:
        # First skip lines until the header line that
        # starts with << Location >> string
        for line in infile:
            if line.startswith("Location"):
                break
        header = line
        for line in infile:
            tokens = line.rstrip().split("\t")
            start = int(tokens[1])
            end = int(tokens[2])
            for pos in range(start, end+1):
                annotation[pos] = tokens

    return annotation

def ParseSNP(snp_fh, field, sep):
    '''GENERATOR: Parse snp_file and yield tuples of lines
    as strings and positions'''

    for line in snp_fh:
        sline = line.rstrip().split(sep)
        pos = int(sline[field])
        yield (line, pos)

def FilterSnps(args):
    import sys

    field = args.field
    field = int(field) - 1
    annotation = LoadAnnotation("/data/Databases/MTB_annotation/H37Rv.annotation.tab")
    with open(args.snp_file) as infile:
        if not args.noheader:
            header = infile.readline()
            sys.stdout.write(header)

        for line, pos in ParseSNP(infile, field, args.sep):
            tokens = annotation[pos]
            if tokens[-1] == "KEEP":
                sys.stdout.write(line)
        sys.stdout.close()

    return 0


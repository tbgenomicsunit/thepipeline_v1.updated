def ParseSNP(snp_fh, field, sep):
    '''GENERATOR: Parse snp_file and yield tuples of lines
    as strings and positions'''

    for line in snp_fh:
        sline = line.rstrip().split(sep)
        pos = int(sline[field])
        yield (line, pos)

def ParseDel(del_file, field, sep, distance):
    '''Return a list with deleted positions parsed from
    .del files'''

    with open(del_file) as del_fh:
        deletions = set() # deleted positions
        # Skip header
        del_fh.readline()
        for line in del_fh:
            line = line.rstrip().split(sep)
            pos = int(line[field])
            deletions.add(pos)
            # Now also include all positions that are "distance" nt
            # away upstream and downstream
            for i in range(1, distance+1):
                deletions.add(pos-i)
                deletions.add(pos+i)

    return deletions

def GetPrefix(infile, prefix="not_provided", sep="."):
    '''Try to get prefix from a file name'''
    if prefix == "not_provided":
        prefix = infile.split(sep)
        # prefix = prefix[0:-1] # remove trailing character
        return prefix
    else:
        return prefix

def FilterByDel(args):
    '''Load deleted positions and filter snp_file
    according to those deletions'''
    import sys
    from shutil import copyfile

    field = args.field
    field = field - 1
    try:
        prefix = GetPrefix(args.snp_file)
    except IOError:
        sys.exit("{} does not exist. Filtering aborted".format(args.snp_file))
    try:
        deletions = ParseDel(args.del_file, field, args.del_sep, args.distance)
    except IOError:
        # Create anyway a delF file that
        copyfile(args.snp_file, "{}.NoDel.delF".format(".".join(prefix)))
        sys.exit("{} does not exist. Filtering aborted".format(args.del_file))

    with open("{}.delF".format(".".join(prefix)), "w") as outfile:
        with open(args.snp_file) as infile:
            header = infile.readline()
            if not header.startswith("Chrom"):
                header = "Chrom\tPosition\tRef\tCons\tReads1\tReads2\tVarFreq\tStrands1\tStrands2\tQual1\tQual2\tPvalue\tMapQual1\tMapQual2\tReads1Plus\tReads1Minus\tReads2Plus\tReads2Minus\tVarAllele\n"
            outfile.write(header)

            for line, pos in ParseSNP(infile, field, args.snp_sep):
                if pos not in deletions:
                    outfile.write(line)

    

    return 0
        
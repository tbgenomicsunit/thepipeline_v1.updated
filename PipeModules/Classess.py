class SNP:

    def __init__(self, type, chrom, pos, ref, alt, r1, r2, freq,
                 s1="NA", s2="NA", q1="NA", q2="NA", pval="NA", mapq1="NA",
                 mapq2="NA", r1plus="NA", r1min="NA", r2plus="NA", r2min="NA",
                 major_var="NA", conferDR=False, knownDR=False, drug = "NA", gene="NA"):
        self.chrom = chrom
        self.position = pos
        self.ref = ref
        self.alt = alt
        self.r1 = int(r1)
        self.r2 = int(r2)
        self.cov = r1 + r2
        self.freq = int(freq.rstrip("%"))
        self.s1 = s1
        self.s2 = s2
        self.q1 = q1
        self.q2 = q2
        self.pval = pval
        self.mapq1 = mapq1
        self.mapq2 = mapq2
        self.r1plus = r1plus
        self.r1min = r1min
        self.r2plus = r2plus
        self.r2min = r2min
        self.major_var = major_var
        self.conferDR = conferDR
        self.knownDR = knownDR
        self.drug = drug
        self.gene = gene

        if self.type = "INDEL":
            if len(self.ref) < len(self.var):
                self.type_indel = "Ins"
                self.indel_length = len(self.var) - len(self.ref)
                self.indel_sequence = self.var[1:]
            elif len(self.ref) > len(self.var):
                self.type_indel = "Del"
                self.indel_length = len(self.ref) - len(self.var)
                self.indel_sequence = self.ref[1:]
            else:
                assert False

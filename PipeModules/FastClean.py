def ReadConfig(cfgfile):
    '''This function read fastP paramters from a config file. If no config file
    is provided, then it reads from ThePipeline/PipeModules/PipeConfigs/
    where parameters are specified like

    TRAILING:10
    SLIDINGWINDOW:25:20
    MINLEN:50

    it returns a list of parameters ready tu build the command line (CMD) to
    call fastP. In the default config file these are the specified parameters
    --cut_by_quality3
    --cut_window_size=10
    --cut_mean_quality=20
    --length_required=50
    --correction
    '''

    import os
    import sys

    parameters = []

    if cfgfile == "default_config":
        modules_dir = os.path.dirname(os.path.abspath(__file__))
        configs_dir = "{}/PipeConfigs".format(modules_dir)
        config = "{}/fastp_default.config".format(configs_dir)
        infile = open(config)
    else:
        try:
            infile = open(cfgfile)
        except IOError:
            sys.exit("Configuration file not found")

    for line in infile:
        if not line.startswith("#"):
            parameter = line.rstrip()
            parameters.append(parameter)

    infile.close()

    return parameters

def BuildCMD(args):
    ''' This function builds a list with the command line (CMD) that must be
    passed to "call" in order to call fastp'''
    import sys
    from Repository import Programs

    # Get fastp PATH. By default: /data/ThePipeline_programs/fastp/fastp
    programs = Programs()
    fastp = programs["fastp"]

    CMD = [ fastp ]

    # Add input and output files to CMD
    if len(args.fastq) == 1 :
        CMD.append("--in1={}".format(args.fastq[0]))
        CMD.append("--out1={}.clean.fastq.gz".format(args.prefix))
    elif len(args.fastq) == 2:
        CMD.append("--in1={}".format(args.fastq[0]))
        CMD.append("--out1={}.P1.clean.fastq.gz".format(args.prefix))
        CMD.append("--in2={}".format(args.fastq[1]))
        CMD.append("--out2={}.P2.clean.fastq.gz".format(args.prefix))
    else:
        sys.exit("You must provide one or two fastq files. Not less; not more")

    # FastP don't allow using more than 16 threads
    if int(args.threads) > 16:
        sys.stderr.write("fastP do not allow more than 16 threads. Threads "
            "parameter has been set to 16")
        args.threads = "16"

    # Check how many threads should been spawned
    CMD.append("--thread")
    CMD.append(args.threads)

    if args.phred64:
        # Specify if Phred +64 scale is being used
        CMD.append("--phred64")

    # Pass the html output name
    CMD.append("--html={}.fastReport.html".format(args.prefix))
    # Now append parameters defined in config file
    parameters = ReadConfig(args.cfgfile)
    CMD.extend(parameters)

    return CMD

def FastClean(args):
    '''Call fastp'''
    from subprocess import call
    import sys
    from History import UpdateHistory

    # Build a list with the command line to call fastP
    fastp_CMD = BuildCMD(args)
    # If verbose is specified, print to the terminal the CMD used to call fastp
    if args.verbose:
        sys.stdout.write("\n{}\n".format(" ".join(fastp_CMD)))

    # When calling fastp, print the output with the summary of the cleaning
    # to PREFIX.cleanlog
    with open("{}.cleanlog".format(args.prefix), "w") as outfh:
        stat = call(fastp_CMD, stdout=outfh, stderr=outfh)
    if stat != 0:
        sys.exit("Pipeline stopped when cleaning with fastp\n"
                 "Stat value: {}\n".format(stat))

    UpdateHistory(fastp_CMD, "fastp", args.prefix)

    return 0

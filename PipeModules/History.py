def UpdateHistory(command, program, prefix):
    '''This function will be called to either update a history_tp.txt file where
    with the last command executed, the directory and the date
    '''
    import time
    import os
    import subprocess as sp
    from Version import version
    # Get the date
    date = time.localtime()
    cwd = os.getcwd()
    v = version(program)

    hist = "{}/{}.history".format(cwd, prefix)
    if os.path.isfile(hist): # Check wheter history_tp already exists for reading
        if os.stat(hist).st_size != 0: # Check that is not empty by some reason
            with open(hist) as infile:
                history = infile.readlines() # Read the file
        else:
            history = None
    else:
        history = None

    with open(hist, "w") as outfile:
        if history:
            for line in history:
                outfile.write(line)

        outfile.write("{};".format(program))
        outfile.write(cwd + ";")
        outfile.write("{}-{}-{}@{}:{};".format(
                                                date.tm_mday,
                                                date.tm_mon,
                                                date.tm_year,
                                                date.tm_hour,
                                                date.tm_min
                                                ))
        outfile.write(" ".join(command)+ ";")
        outfile.write(v)
        outfile.write("\n")

    return 0

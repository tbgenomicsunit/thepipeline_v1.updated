def IndexBAM(prefix, ext):
    ''' Index the bam file so GATK can do de variant calling'''
    from Repository import Programs
    import subprocess as sp
    import sys

    programs = Programs()
    samtools = "/data/ThePipeline_v2/Programs/samtools-1.15/samtools"
    stat = sp.call([samtools, "index", "{}{}".format(prefix, ext)])
    if stat != 0:
        sys.exit("Pipeline stopped when indexing bam file\n")

    return 0

def VarScan(reference, prefix, varscan, samtools, ext):
    '''Call samtools mpileup and Varscan with bamfile'''
    from subprocess import call
    import sys
    from History import UpdateHistory

    bamfile = "{}{}".format(prefix, ext)
    with open("{}.mpileup.remove".format(prefix), "w") as outfh:
        print "CMD = {}".format(" ".join([samtools, "mpileup", "-AB", "-f",
                                          reference, bamfile]))
        cmd = [samtools, "mpileup", "-AB", "-f", reference, bamfile]
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at samtools mpileup!\n")
        UpdateHistory(cmd, "samtools", prefix)

    with open("{}.snp".format(prefix), "w") as outfh:
        cmd = ["java", "-Xms10G", "-Xmx32G", "-jar", varscan,
            "pileup2snp", "{}.mpileup.remove".format(prefix),
            "--min-coverage", "3",
             "--min-reads2", "3",
             "--min-freq-for-hom", "0.90",
             "--min-var-freq", "0.05"]
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at VarScan pileup2snp!\n")
        UpdateHistory(cmd, "varscan", prefix)

    with open("{}.varscan.snp.indel".format(prefix), "w") as outfh:
        cmd = ["java", "-Xms10G", "-Xmx32G", "-jar", varscan,
            "pileup2indel", "{}.mpileup.remove".format(prefix),
            "--min-coverage", "10",
             "--min-freq-for-hom", "0.90",
             "--min-var-freq", "0.10"]
        stat = call(cmd,stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at VarScan pileup2indel!\n")
        UpdateHistory(cmd, "varscan", prefix)

    with open("{}.DR.snp.indel".format(prefix), "w") as outfh:
        cmd = ["java", "-Xms10G", "-Xmx32G", "-jar", varscan,
            "pileup2indel", "{}.mpileup.remove".format(prefix),
            "--min-coverage", "10",
             "--min-freq-for-hom", "0.90",
             "--min-var-freq", "0.10"]
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at VarScan pileup2indel!\n")
        UpdateHistory(cmd, "varscan", prefix)

    with open("{}.DR.snp.final".format(prefix), "w") as outfh:
        cmd = ["java", "-Xms10G", "-Xmx32G", "-jar", varscan,
            "filter", "{}.snp".format(prefix),
            "--p-value", "0.01",
             "--min-reads2", "6",
             "--min-coverage", "10",
             "--min-avg-qual", "25",
             "--min-strands2", "2",
             "--min-var-freq", "0.10"]
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at VarScan filter\n")
        UpdateHistory(cmd, "varscan", prefix)

    with open("{}.EPI.snp.nodensityfilter".format(prefix), "w") as outfh:
        cmd = ["java", "-Xms10G", "-Xmx32G", "-jar", varscan,
            "filter", "{}.snp".format(prefix),
            "--p-value", "0.01",
            "--min-coverage", "20",
             "--min-reads2", "20",
             "--min-avg-qual", "25",
             "--min-strands2", "2",
             "--min-var-freq", "0.90"]
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at VarScan filter\n")
        UpdateHistory(cmd, "varscan", prefix)
    return 0

def Calling_Subscript_21(prefix, wdensity, windel):
    '''Call subscript that includes awk and Rscript lines to filter snps and
    generate .snp.final'''
    import os
    import sys
    from subprocess import call

    path =  os.path.dirname(os.path.abspath(__file__))
    script_DR = "{}/PipeScripts/Calling_Subscript.DR.sh".format(path)
    script_EPI = "{}/PipeScripts/Calling_Subscript.EPI.sh".format(path)
    rscript = "{}/PipeScripts/2_1_filtering_ThePipeline.R".format(path)
    stat = call(["bash", script_DR, prefix, rscript])
    if stat != 0:
        sys.exit("Pipeline stopped at Calling_Subscript.DR.sh\n")
    stat = call(["bash", script_EPI, prefix, rscript, wdensity, windel])
    if stat != 0:
        sys.exit("Pipeline stopped at Calling_Subscript.EPI.sh\n")

    return 0

#def clean_files(prefix):
 #   '''clean .mpileup.remove files once snp calling has finished'''
 #   import os
 #   import glob

    #for file in glob.glob("{}.*.remove".format(prefix)):
    #    os.remove(file)

 #   return 0

def check_extension(ext):
    import sys

    if ext.endswith(".bam") or ext.endswith(".cram"):
        return 0
    else:
        sys.exit("GATK only accepts files ended in .bam or .cram extension")

def Calling(args):
    from Repository import Programs, Data
    from IndelCalling import IndelCalling
    from IndelFormatting import VCF2VarScan_file

    programs = Programs()
    data = Data()

    samtools = "/data/ThePipeline_v2/Programs/samtools-1.15/samtools"
    varscan = programs["VarScan"]
    reference = args.reference
#    bamfile = args.bamfile

    # Check that mapping files end with .bam/.cram
    check_extension(args.ext)

    if not reference:
        reference = data["reference"]

    # SNP calling with VarScan
    VarScan(reference, args.prefix, varscan, samtools, args.ext)

    # Index bam with samtools
    IndexBAM(args.prefix, args.ext)

    # Call and filter indels with GATK
    IndelCalling(args.prefix, args.ext, reference)

    #Convert GATK vcf files to VarScan legacy files
    VCF2VarScan_file(args.prefix, args.indelfreq)


    Calling_Subscript_21(args.prefix, args.wdensity, args.windel)

    if args.noclean:
        return 0
    else:
        clean_files(args.prefix)

    return 0

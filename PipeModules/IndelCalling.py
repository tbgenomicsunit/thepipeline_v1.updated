def IndelCalling(prefix, ext, reference):
    '''Perform the indel calling and filtering using GATK'''
    from Repository import Programs, Data
    import subprocess as sp
    import sys
    from History import UpdateHistory

    programs = Programs()
    data = Data()
    gatk = programs["gatk"]

    # INDEL/SNP CALLING
    cmd = ["java", "-Xmx4g", "-jar", gatk, "-T", "HaplotypeCaller", "-R",
             reference, "-I", "{}{}".format(prefix, ext), "-ploidy", "1",
             "-o", "{}.gatk.vcf".format(prefix)]
    stat = sp.call(cmd)
    if stat != 0:
        sys.exit("Pipeline stopped at HaplotypeCaller (GATK)")
    UpdateHistory(cmd, "gatk", prefix)

    # INDEL SELECTION
    cmd = ["java", "-Xmx4g", "-jar", gatk, "-T", "SelectVariants", "-R",
             reference, "-V", "{}.gatk.vcf".format(prefix), "-selectType",
             "INDEL", "-o", "{}.gatk.indel.vcf".format(prefix)]
    stat = sp.call(cmd)
    if stat != 0:
        sys.exit("Pipeline stopped at SelectVariants (GATK)")
    UpdateHistory(cmd, "gatk", prefix)

    # INDEL FILTER
    cmd = ['java', '-Xmx4g', '-jar', gatk, '-T', 'VariantFiltration', '-R',
             reference, '-V', '{}.gatk.indel.vcf'.format(prefix), '--filterExpression',
             'QD<2.0', '--filterName', '"LowQD"', '--filterExpression', 'FS>200.0',
             '--filterName', '"HighFS"', '--filterExpression', 'SOR>3.0',
             '--filterName',  '"HighSOR"', '--filterExpression', 'MQ<40.0',
             '--filterName', '"Low40MQ"', '--filterExpression',  'ReadPosRankSum<-20.0',
             '--filterName', '"LowReadPRS"', '--filterExpression',  'DP<20',
             '--filterName', '"LowDepth"', '-o',  '{}.gatk.indel.filter.vcf'.format(prefix)]
    stat = sp.call(cmd)

    if stat != 0:
        sys.exit("Pipeline stopped at VariantFiltration (GATK)")
    UpdateHistory(cmd, "gatk", prefix)
    
    return 0

#Search for resistance and linaje
#For running  Rscript resistance_phylogney_2.2.R high_confidence_phyrese.csv snp_phylo.csv [path to folder with all the snp.final files]
#output is resistance_phylogeny_result

args=commandArgs(trailingOnly = TRUE)
print("Search for SNPs related with antibiotic resistance")
resis_pannel <- read.delim(args[1], header = TRUE, stringsAsFactors=FALSE) #panel de resistencias se puede modificar pero...¡mantened el formato!
resis_pannel[,6]=toupper(resis_pannel[,6])
phylo_pannel <- read.delim(args[2], header = TRUE, stringsAsFactors=FALSE) #panel de snps de filogenia
archivos = list.files(args[3],pattern="DR.snp.final$") #extraigo los snp.final de la carpeta

result=matrix(nrow=length(archivos), ncol=length(unique(resis_pannel[,'Antibiotic']))) #crea la matriz de resultados
result=cbind(result,NA)
colnames(result)=c(unique(resis_pannel[,'Antibiotic']),'Phylogeny') #añado la columna Phylogeny
columns_order=c("streptomycin (SM)", "isoniazid (INH)", "rifampicin (RMP)", "ethambutol (EMB)", "pyrazinamide (PZA)","fluoroquinolones (FQ)","amikacin (AMK) kanamycin (KAN) capreomycin (CPR)","isoniazid (INH) ethionamide (ETH)", "ethionamide (ETH)","para-aminosalicylic acid (PAS)","linezolid (LZD)","kanamycin (KAN)","capreomycin (CPR)","Phylogeny")
result=result[,columns_order] #cambiar el orden para que sea igual al que le interesa a Irving
colnames(result)[7]="amikacin (AMK)"
result=result[,-8]
rownames(result)=archivos

for(i in 1:length(archivos)) #para todos los snp.final
{
  snp_file=read.delim(paste(args[3],archivos[i],sep='') , stringsAsFactors=FALSE) #fichero .snp
  snp_res=which(is.element(snp_file[,'Position'],resis_pannel[,1])) #que snps de snp.final estan asociados a resistencia
  result[i,-dim(result)[2]]="Susceptible" #por defecto, rellena con susceptible
  if (length(snp_res)>0) #si hay algún snp de resistencia...
  {
    table_res=matrix(data=0, nrow=length(snp_res), ncol=length(unique(resis_pannel[,'Antibiotic']))) #inicio la tabla de resistencias que irá en otro fichero
    table_res=cbind(snp_file[snp_res,'Position'],table_res,0,NA,NA,NA, NA,NA)
    colnames(table_res)=c('Position', colnames(result)[-14],'Freq', 'WT','MUT', 'Codon_change','AA_change','Source')
    for(j in 1:length(snp_res))
    {
      if (resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2]),'Antibiotic']=="amikacin (AMK) kanamycin (KAN) capreomycin (CPR)") { #por si tenemos multiresistencia en 1 SNP
          result[i,'amikacin (AMK)']="Resistant"
          result[i,'kanamycin (KAN)']="Resistant"
          result[i,'capreomycin (CPR)']="Resistant"
          table_res[j,'amikacin (AMK)']=1
          table_res[j,'kanamycin (KAN)']=1
          table_res[j,'capreomycin (CPR)']=1
        } else { if (resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2]),'Antibiotic']=="isoniazid (INH) ethionamide (ETH)") { #por si tenemos multiresistencia en 1 SNP
                    result[i,'isoniazid (INH)']="Resistant"
                    result[i,'ethionamide (ETH)']="Resistant"
                    table_res[j,'isoniazid (INH)']=1
                    table_res[j,'ethionamide (ETH)']=1
        } else {result[i,resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2]),'Antibiotic']]="Resistant"
                table_res[j,resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2]),'Antibiotic']]=1} #La tabla ampliada. Pone la información de que resistencia está asociada al SNP
      }#busca los snps de resistencia que están en el snp.final y la columna de la tabla correspondiente la pone a Resistant. ¡Solo mira posición, no alelo!
      table_res[j,'WT']=snp_file[snp_res[j],'Ref']
      table_res[j,'MUT']=snp_file[snp_res[j],'VarAllele']
      table_res[j,'Freq']=sub("%","",snp_file[snp_res[j],'VarFreq'])
      if (length(which(resis_pannel[,1]==snp_file[snp_res[j],2] & resis_pannel[,6]==snp_file[snp_res[j],'VarAllele'])) > 0) 
        {
        table_res[j,'Source']=resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2] & resis_pannel[,6]==snp_file[snp_res[j],'VarAllele']),'Source'] #si algun SNP coincide en posicion y alelo, escríbelo
        table_res[j,'Codon_change']=resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2] & resis_pannel[,6]==snp_file[snp_res[j],'VarAllele']),'Codon.change']
        table_res[j,'AA_change']=resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2] & resis_pannel[,6]==snp_file[snp_res[j],'VarAllele']),'AA.change']
        
      } else { table_res[j,'MUT']=paste(snp_file[snp_res[j],'VarAllele'], " (NEW)", sep='')
               table_res[j,'Source']=paste(resis_pannel[which(resis_pannel[,1]==snp_file[snp_res[j],2]),'Source'], collapse = ' ') } #si coincide en la posicion pero no en el alelo
    }
    write.table(table_res, file=paste(archivos[i],"_snps_result", sep=''),quote=FALSE, row.names = FALSE, sep='\t')
  }
  snp_filo=which(is.element(as.numeric(phylo_pannel[,2]),snp_file[,2])) #que snps de snp.final son de filogenia
  if (length(snp_filo)==0) {result[i,'Phylogeny'] = "Unknown"} #si no encuentra ninguno, pone unknown
  else {result[i,'Phylogeny']=phylo_pannel[snp_filo[length(snp_filo)],'lineage']} #el que encuentre, pone el linaje más "profundo". Por ejemplo, en vez de L4, L4.3.2.1
}

write.table(result, file="resistance_phylogeny_result",col.names=NA,quote=FALSE, sep='\t')


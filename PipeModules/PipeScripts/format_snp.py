import sys

with open("H37Rv.annotation.tab") as infile:
	lines = infile.readlines()
	header = lines[2]
	sys.stdout.write(header)
	for i in xrange(3, len(lines) - 6):
		prev = lines[i]
		curr = lines[i+1]
		nex = lines[i+2]
		tok_prev = prev.rstrip().split("\t")
		tok_curr = curr.rstrip().split("\t")
		tok_next = nex.rstrip().split("\t")
		if tok_curr[3] == "I":
			if tok_prev[-1] == "DISCARD":
				tok_curr[-1] = "DISCARD"
			if tok_next[-1] == "DISCARD":
				tok_curr[-1] = "DISCARD"
		line = "{}"
		line += "\t{}" * (len(tok_curr) - 1)
		line += "\n"
		sys.stdout.write(line.format(*tok_curr))

#! /bin/bash

# Script to extract the nonredundant list of SNP position HQ-homozygous calls, 
# the script 1. extracts the nonredudant list; 2. annotates the positions; 3.
# removes the problematic regions (PE/PPE, phage/mobile, repeats) and generates 
# the file for the next step: cluster.positions 

# Me doy cuenta de que en los archivos DR final, las posiciones heterozigotas
# estan marcadas con nucleotidos ambiguos como K, Y, S... , y puesto que tengo
# prisa en analizar las muestras de Georgia vamos a hacer un dirty fix que consiste
# en poner en la 4 columna donde estan los nucleotidos ambiguos, la columna 19
# que tiene el nucleotido alternativo mas frecuente

cat *DR.snp.final | 
  awk -v OFS="\t" '{$4=$19; print}' | # ESTA ES LA LINEA DIRTY FIX
  grep -v Position | 
    sort -k2,2n -k4 | 
      awk '{print $1, $2, ".", $3, $4, ".", ".", "."}' | 
        sort -u -k2,2n  | 
           sed 's/ /\t/g' > DR.nonredudant.positions.alleles

java -jar /data/ThePipeline_programs/snpEff/snpEff.jar -v -hgvs1LetterAa -noStats -no-downstream -no-intergenic -no-intron -no-upstream -noLof MTB_ancestor DR.nonredudant.positions.alleles | 
        grep -o '^[^#]*' | 
           sed 's/|/\t/g' > DR.result_snpeff.vcf

Rscript /data/ThePipeline/PipeModules/PipeScripts/3_1_filter_snpeff_result.R DR.result_snpeff.vcf /data/ThePipeline/PipeModules/PipeScripts/H37Rv_annotation2sytems.ptt DR.nonredudant.positions.alleles DR.SNP_table_final.txt

# Hasta aquí la parte del script que ha hecho Álvaro, donde ha sacado la lista
# de posiciones no redundantes y las ha anotado. Lo que vamos a hacer ahora es
# eliminar las que estan anotadas como PPE, repeat y/o phage y luego filtrar
# cada archivo *.snp.final de manera que se descarte cualquier posicion que no
# esté en el table_final.txt

# Quitamos las regiones que no nos interesan y guardamos la lista de posiciones
# filtradas
grep -v PPE DR.SNP_table_final.txt | 
  grep -vi phage | 
    grep -vi repeat |
      cut -f2 > DR.filtered.final.positions
        


cat *EPI.snp.final | 
  awk -v OFS="\t" '{$4=$19; print}' |
  grep -v Position | 
    sort -k2,2n -k4 | 
      awk '{print $1, $2, ".", $3, $4, ".", ".", "."}' | 
        sort -u -k2,2n  | 
           sed 's/ /\t/g' > EPI.nonredudant.positions.alleles

java -jar /data/ThePipeline_programs/snpEff/snpEff.jar -v -hgvs1LetterAa -noStats -no-downstream -no-intergenic -no-intron -no-upstream -noLof MTB_ancestor EPI.nonredudant.positions.alleles |
        grep -o '^[^#]*' | 
           sed 's/|/\t/g' > EPI.result_snpeff.vcf

Rscript /data/ThePipeline/PipeModules/PipeScripts/3_1_filter_snpeff_result.R EPI.result_snpeff.vcf /data/ThePipeline/PipeModules/PipeScripts/H37Rv_annotation2sytems.ptt EPI.nonredudant.positions.alleles EPI.SNP_table_final.txt

# Quitamos las regiones que no nos interesan y guardamos la lista de posiciones
# filtradas
grep -v PPE EPI.SNP_table_final.txt | 
  grep -vi phage | 
    grep -vi repeat |
      cut -f2 > EPI.filtered.final.positions
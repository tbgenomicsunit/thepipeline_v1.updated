def Programs():
    ''' Parse .programs_path and return a dictionary with programs paths like
    bwa = /data/Software/bwa/bwa.1.23 '''
    import os

    path = os.path.dirname(os.path.abspath(__file__))  
    programs = {}
    with open("{}/PipePaths/programs_path".format(path)) as infile:
        for line in infile:
            line = line.rstrip()
            program, path = line.split("=")
            programs[program] = path

    return programs

def Data():
    ''' Parse .data_path and return a dictionary with data files paths like:
    data["reference"] = /data/genome_epi_val/MTB_ancestor.fasta '''
    import os

    path = os.path.dirname(os.path.abspath(__file__))  
    data = {}
    with open("{}/PipePaths/data_path".format(path)) as infile:
        for line in infile:
            line = line.rstrip()
            file, path = line.split("=")
            data[file] = path

    return data
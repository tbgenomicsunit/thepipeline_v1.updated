def ReadConfig(cfgfile):
    import os
    import sys

    parameters = []

    if cfgfile == "default_config":
        modules_dir = os.path.dirname(os.path.abspath(__file__))
        configs_dir = "{}/PipeConfigs".format(modules_dir)
        config = "{}/trimmomatic_default.config".format(configs_dir)
        infile = open(config)
    else:
        try:
            infile = open(cfgfile)
        except IOError:
            sys.exit("Configuration file not found")

    for line in infile:
        if not line.startswith("#"):
            parameter = line.rstrip()
            parameters.append(parameter)

    infile.close()

    return parameters

def BuildCMD(args):
    import sys
    from Repository import Programs
    # Get Trimmomatic PATH
    programs = Programs()
    trimmomatic = programs["trimmomatic"]

    CMD = ["java", "-jar", trimmomatic ]

    # Check if it has to be run in SingleEnd or PairedEnd mode
    if len(args.fastq) == 1 :
        tmode = "SE"
    elif len(args.fastq) == 2:
        tmode = "PE"
    else:
        sys.exit("You must provide one or two fastq files. Not less; not more")

    CMD.append(tmode)

    # Check how many threads should been spawned
    CMD.append("-threads")
    CMD.append(args.threads)
    # Specify that Phred +33 scale is being used
    CMD.append("-phred33")


    CMD.append("-trimlog")
    CMD.append("{}.trimlog".format(args.prefix))

    # Append input and output files
    if tmode == "SE":
        CMD.append(args.fastq[0])
        CMD.append("{}.clean.fastq.gz".format(args.prefix))
    else:
        CMD.extend(args.fastq)
        CMD.append("{}.P1.clean.fastq.gz".format(args.prefix))
        CMD.append("{}.U1.clean.fastq.gz".format(args.prefix))
        CMD.append("{}.P2.clean.fastq.gz".format(args.prefix))
        CMD.append("{}.U2.clean.fastq.gz".format(args.prefix))

    # Now append parameters defined in config file
    parameters = ReadConfig(args.cfgfile)
    CMD.extend(parameters)

    return CMD

def TrimClean(args):
    '''Call required parameters for clean files'''
    from subprocess import call
    import sys
    from History import UpdateHistory

    trim_CMD = BuildCMD(args)
    sys.stdout.write("{}\n".format(trim_CMD))
    with open("{}.cleanlog".format(args.prefix), "w") as outfh:
        stat = call(trim_CMD, stdout=outfh, stderr=outfh)
    if stat != 0:

        sys.exit("Pipeline stopped when cleaning with trimmomatic\n"
                 "Stat value: {}\n".format(stat))
    UpdateHistory(trim_CMD, "trimmomatic", args.prefix)

    return 0

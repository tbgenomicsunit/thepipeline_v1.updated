def ParseVCF(prefix):
    '''Parse VCF files'''

    with open("{}.gatk.indel.filter.vcf".format(prefix)) as infh:
        for line in infh:
            if not line.startswith("#"):
                line = line.rstrip().split("\t")
                yield line

def getFreq(vcf_sample):
    '''Get the sample field of an VCF and calculate position depth and variant
    frequencies'''

    GT, AD, DP, GQ, PL =  vcf_sample.split(":")
    ref_depth, alt_depth = AD.split(",")
    DP = float(DP)
    if not DP:
        return (0.0, 0.0, 0.0, 0.0)

    ref_depth = float(ref_depth)
    alt_depth = float(alt_depth)
    alt_freq = alt_depth / DP

    return (ref_depth, alt_depth, alt_freq, DP)

def filterVariant(vcf_line, alt_freq_cutoff):
    ''' Given a vcf line and a set of filters defined by user return True if
    the variant pass all filters or False otherwise

    Apart, I have to implement here a Depth filter as GATK does not filter as we
    expected. So ref_depth + alt_depth must be at least 20
    '''
    chrom, pos, ref, alt, ref_depth, alt_depth, alt_freq, qual, filt = vcf_line

    if alt_freq < alt_freq_cutoff :
        return False
    if filt != "PASS":
        return False
    if ref_depth + alt_depth < 20:
        return False

    return True

def VCF2VarScanFMT(vcf_line):
    '''Convert a line of an VCF parsed from GATK to a VarScan format to be
    compatible with our pipeline and write the .indel file.

    line fields are stored in lists.
    '''

    chrom, pos, ref, alt, ref_depth, alt_depth, alt_freq, qual, filt = vcf_line
    VarScan_line = [
                    chrom,
                    pos,
                    ref,
                    alt,
                    ref_depth,
                    alt_depth,
                    alt_freq,
                    qual,
                    qual,
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    alt
                   ]

    return VarScan_line

def VCF2VarScan_file(prefix, alt_freq_cutoff):
    '''Given a vcf file, apply filters defined by user and write output in
    VarScan format

    This module is developed to filter and write indels called with GATK.
    Filters applied are a minimum variant frequency and filters previously
    applied in GATK, so all variants that do not "PASS" as per GATK are
    filtered as well


    '''

    with open("{}.snp.indel".format(prefix), "w") as outfh:
        outfh.write("Chrom\tPosition\tRef\tCons\tReads1\tReads2\tVarFreq\tStrands1\tStrands2\tGATKQual1\tGATKQual2\tPvalue\tMapQual1\tMapQual2\tReads1Plus\tReads1Minus\tReads2Plus\tReads2Minus\tVarAllele\n")
        for line in ParseVCF(prefix):
            chrom, pos, ID, ref, alt, qual, filt, info, fmt, sample = line
            ref_depth, alt_depth, alt_freq, DP = getFreq(sample)
            vcf_line = chrom, pos, ref, alt, ref_depth, alt_depth, alt_freq, qual, filt
            if filterVariant(vcf_line, alt_freq_cutoff):
                VarScan_line = VCF2VarScanFMT(vcf_line)
                # Vamos a anyadir el simbolo de % para que no se nos quejen
                # y los reads1/2 psarlos a int
                VarScan_line[4] = int(VarScan_line[4])
                VarScan_line[5] = int(VarScan_line[5])
                alt_freq = VarScan_line[6]
                alt_freq = 100 * alt_freq
                alt_freq = str(alt_freq)
                alt_freq += "%"
                VarScan_line[6] = alt_freq
                VarScan_line = [ str(item) for item in VarScan_line ]
                VarScan_line = "\t".join(VarScan_line)
                outfh.write(VarScan_line + "\n")

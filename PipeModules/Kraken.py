def KrakenClassify(fastq, krakenDB, prefix, paired, threads, compressed, kraken, preload):
    '''Run kraken to classify fastq reads using krakenDB and store results
    in prefix.kraken file'''
    from subprocess import call
    import sys
    from History import UpdateHistory
    cmd = [ kraken, "--db", krakenDB,
            "--fastq-input", "--threads", threads ]
    if compressed:
        cmd.append("--gzip-compressed")
    if preload:
        cmd.append("--preload")
    if paired:
        cmd.append("--paired")

    for fq in fastq:
        cmd.append(fq)

    with open("{}.kraken".format(prefix), "w") as outfh:
        stat = call(cmd, stdout=outfh)

    if stat != 0:
        sys.exit("Pipeline stopped when classifying reads with Kraken!\n")

    UpdateHistory(cmd, "kraken", prefix)
    return 0

def KrakenTranslate(krakenDB, prefix, kraken_translate):
    ''' Translate .kraken file to .labels file '''
    from subprocess import call
    import sys
    from History import UpdateHistory

    cmd = [ kraken_translate, "{}.kraken".format(prefix), "--db", krakenDB]
    with open("{}.labels".format(prefix), "w") as outfh:
        stat = call(cmd, stdout=outfh)

    if stat != 0:
        sys.exit("Pipeline stopped when translating kraken file!\n")

    UpdateHistory(cmd, "kraken", prefix)
    return 0

def MakeReadList(mstring, prefix):
    '''Call grep to select those reads that match the string
    This string is Mycobacterium tuberculosis by default'''
    import subprocess
    import sys
    from History import UpdateHistory

    cmd = ["/usr/bin/fgrep", mstring, "{}.labels".format(prefix), "|",
           "cut", "-f1", ">", "{}.filtered.readlist".format(prefix)]
    ps = subprocess.Popen(("/usr/bin/fgrep", mstring, "{}.labels".format(prefix)),
        stdout=subprocess.PIPE)
    output = subprocess.check_output(("cut", "-f1"), stdin=ps.stdout)
    ps.wait()

    with open("{}.filtered.readlist".format(prefix), "w") as outfh:
        outfh.write(output)

    UpdateHistory(cmd, "kraken", prefix)
    return 0

def PickReads(fastq, prefix, seqtk, strand):
    '''Write reads in readlist to output'''
    from subprocess import call
    import sys
    from History import UpdateHistory

    readlist = "{}.filtered.readlist".format(prefix)
    cmd = [seqtk, "subseq", fastq, readlist]
    with open("{}{}.filtered.fastq".format(prefix, strand), "w") as outfh:
        stat = call(cmd, stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped when selecting read from readlist!\n")

    UpdateHistory(cmd, "seqtk", prefix)
    return 0

def clean_files(prefix):
    '''Clean .kraken and .labels files once .filtered.readlist file
    has been created'''
    import os

    os.remove("{}.kraken".format(prefix))
    os.remove("{}.labels".format(prefix))

    return 0

def CountReads(prefix):
    '''Count total reads analyzed and MTBC-classified reads'''
    from subprocess import check_output
    import sys

    try:
        with open("{}.nfilter".format(prefix), "w") as outfile:
            analyzed = check_output(["wc", "-l", "{}.kraken".format(prefix)])
            analyzed = analyzed.split()[0]
            MTBC = check_output(["wc", "-l", "{}.filtered.readlist".format(prefix)])
            MTBC = MTBC.split()[0]

            outfile.write("TOTAL:{}\n".format(analyzed))
            outfile.write("MTBC:{}\n".format(MTBC))
            return 0

    except:
        sys.exit("Either .kraken or .filtered.readlist files do not exist")

    assert False

def MakeReport(prefix, krakenDB, version=True):
    '''This function calls kraken-report if specified'''
    import subprocess as sp
    from History import UpdateHistory
    
    cmd = ["/data/ThePipeline_programs/Kraken/kraken-report",
        "--db", krakenDB, "{}.kraken".format(prefix)]
    with open("{}.kraken.report".format(prefix), "w") as outfh:
        stat = sp.call(cmd, stdout=outfh)

    if stat != 0:
        sys.exit("Pipeline stopped when making the kraken report!\n")

    UpdateHistory(cmd, "kraken", prefix)
    return 0

def Kraken(args):
    ''' Launch all function to classify with kraken, translate labels and
    pick reads matching mstring'''
    from Repository import Programs, Data
    import sys
    programs = Programs()
    data = Data()

    fastq = args.fastq
    compressed = args.compressed
    paired = args.paired
    mstring = args.matching
    prefix = args.prefix
    threads = args.threads
    krakenDB = args.krakenDB
    classify = args.classify
    filterf = args.filterf
    if not krakenDB:
        krakenDB = data["krakenDB"]
    preload = args.preload

    kraken = programs["kraken"]
    kraken_translate = programs["kraken_translate"]
    seqtk = programs["seqtk"]

    if not classify and not filterf:
        if not args.report:
            sys.exit("\nThePipeline kraken must be called with at least one"
                " of the following options:\n --classify/--filter/--report. "
                "All can be used at the same time\n")
    if classify:
        KrakenClassify(fastq, krakenDB, prefix, paired, threads, compressed, kraken, preload)
        if args.nolabels == False:
            KrakenTranslate(krakenDB, prefix, kraken_translate)
            MakeReadList(mstring, prefix)
            CountReads(prefix)

    if filterf:
        if len(fastq) == 1:
            PickReads(fastq[0], prefix, seqtk, strand="")
        elif len(fastq) == 2:
            PickReads(fastq[0], prefix, seqtk, strand=".P1")
            PickReads(fastq[1], prefix, seqtk, strand=".P2")
        else:
            assert False

    # If a report is desired, then kall kraken-report to make a report from .kraken
    if args.report:
        MakeReport(prefix, krakenDB)

    if args.noclean:
        return 0
    else:
        try:
            clean_files(prefix)
            return 0
        except OSError:
            # We're trying to remove kraken and labels files
            # but maybe they don't exist
            return 0

    assert False

def KReportParser(report_file, taxlevel):
    '''Parse a kraken-report file and yield
    a list of parsed lines at taxlevel'''
    report_lines = []
    with open(report_file) as infile:
        for line in infile:
            line = line.rstrip()
            line = line.split("\t")
            freq, r1, r2, taxlvl, taxid, name = line
            freq = float(freq)
            name = name.strip()
            if taxlvl == taxlevel:
                report_lines.append((freq, r1, r2, taxlvl, taxid, name))
            elif taxlvl == "-" and name == "Mycobacterium tuberculosis complex":
                report_lines.append((freq, r1, r2, taxlvl, taxid, name))
            elif taxlvl == "U":
                report_lines.append((freq, r1, r2, taxlvl, taxid, name))

    return report_lines

def KrakenReport(args):
    '''Launch kraken-report, parsing a kraken-report file and producing a tabular
    and plotable table'''
    import sys

    MTBC = False # Control if there is MTBC in the sample
    report_lines = KReportParser(args.report_file, args.taxlevel)
    # Sort for descending freq
    report_lines = sorted(report_lines, key=lambda x:x[0], reverse=True)
    for line in report_lines:
        freq, r1, r2, taxlvl, taxid, name = line
        if name == "Mycobacterium tuberculosis complex":
            MTBC = True
            MTBC_freq = freq
        elif name == "Mycobacterium":
            Myco_freq = freq
        sys.stdout.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
            freq,
            r1,
            r2,
            taxlvl,
            taxid,
            name,
            args.prefix))
    # If there was MTBC in sample, calc the difference between Mycobacterium
    # and MTBC, to check if there is any Non tuberculous mycobacteria that
    # is contaminating the sample
    if MTBC and args.taxlevel == "G":
        NTM_freq = Myco_freq - MTBC_freq
        sys.stdout.write("{}\tNA\tNA\t-\t-\tNTM\t{}\n".format(NTM_freq, args.prefix))

    return 0

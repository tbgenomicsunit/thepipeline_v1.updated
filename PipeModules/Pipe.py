def Pipeit(args):
	'''Call all modules needed to perform a complete analysis '''
	import ListFiles
	from subprocess import call 

	suffix = args.suffix
	folder = args.folder
	kraken = args.kraken
	compressed = args.compressed
	paired = args.paired
	density = args.density

	# Get list of files to process (paired or single)
	if paired:
		files = ListFiles.ListPaired(folder, suffix)
	else:
		files = ListFiles.ListSingle(folder, suffix)

	if kraken:
		firstfile = files.pop()
		cmd = ["ThePipeline", "kraken", ]
def SNP_parser(snp_file):
    '''GENERATOR: parse a snp_file filehandler and yield lines with included
    fields'''

    # Skip header
    snp_file.readline()

    for line in snp_file:
        # Get a list of all fields in line
        line = line.rstrip()
        line = line.split("\t")
        # Get position
        pos = line[1]
        ref = line[2]
        # Instead of getting var that can be W, R, S... I'll pick the majority variant
        # var = line[3]
        var = line[18]
        freq = line[6]

        yield (pos, ref, var, freq)

def BuildMergeDict(snp_files):
    '''Iterate over snp_files to be merged and build the merged dict as they
    are parsed

    merge_dict = { position1 : "C\tT" : { Sample1: 93.7%, Sample2: 98%, ... },
                               "C\tG" : { Sample3: 91.2%, Sample4: 45%, ... }

                 }
    '''
    import sys

    merge_dict = {}
    deletions = {}
    for snp_file in snp_files:
        # Get the sample name assuming is separated by dot
        sample = snp_file.split(".")[0]
        with open(snp_file) as infile:
            for pos, ref, var, freq in SNP_parser(infile):
                pos = int(pos) # So we can numerically sort them
                refvar = "{}\t{}".format(ref, var)
                # If that position has not been observed yet
                if pos not in merge_dict:
                    merge_dict[pos] = { refvar : { sample : freq } }
                # If pos has already been observed, just update the dict
                # with the new line
                else:
                    # Bu we also have to check if a single position has more
                    # than one variant in a single Sample
                    if refvar not in merge_dict[pos]:
                        merge_dict[pos][refvar] = { sample : freq }
                    else:
                        merge_dict[pos][refvar].update({ sample : freq })
        try:
            with open("{}.snp.del".format(sample)) as delfh:
                header = delfh.readline()
                deletions[sample] = {}
                for line in delfh:
                    c, pos, foo, bar = line.split()
                    deletions[sample][int(pos)] = None
        except IOError:
            sys.exit("The file {}.snp.del cannot be open".format(sample))



    return merge_dict, deletions

def WriteMergeDict(args):
    '''Build and write the merged dict between SNP files'''
    import sys

    merge_dict, deletions = BuildMergeDict(args.snp_files)
    # We need to know all positions that have been observed and sort them as
    # a Python dict is unordered by definition but we want our output to be
    # sorted
    obs_positions = [ pos for pos in merge_dict ]
    obs_positions.sort() # Sort positions
    # get sample names to build the HEADER
    header = "Position\tRef\tVarAllele"
    samples = [ name.split(".")[0] for name in args.snp_files ]
    # Write header
    sys.stdout.write("{}\t{}\n".format(header, "\t".join(samples)))
    for pos in obs_positions:
        for refvar in merge_dict[pos]:
            line = "{}\t{}".format(pos, refvar)
            for sample in samples:
                # For a position in a sample, if a variant has been observed
                # its frequency will be assigned. However, if afterwards a
                # different variant in the same position has NOT been observed
                # the position will be tagged as NA. To solve this I should add
                # A boolean to know if any variant has been already observed
                # for a position.
                var_observed = False
                if sample in merge_dict[pos][refvar]:
                    varfreq = merge_dict[pos][refvar][sample]
                    var_observed = True
                elif sample in deletions and pos in deletions[sample]:
                    varfreq = "DEL"
                elif var_observed == False:
                    varfreq = "NA"
                else:
                    assert False
                line += "\t{}".format(varfreq)
            line += "\n"
            sys.stdout.write(line)

    return 0

#! /bin/bash

ls *.meancov | cut -f1 -d '.' | while read PREFIX
  do
    # Try to stat EPI.snp.final
    stat $PREFIX.EPI.snp.final > /dev/null
    # If file does not exist, create one with header
    if [ $? != 0 ]
      then
         echo "Chrom   Position        Ref     Cons    Reads1  Reads2  VarFreq Strands1        Strands2        Qual1   Qual2   Pvalue  MapQual1        MapQual2        Reads1Plus      Reads1Minus     Reads2Plus      Reads2Minus     VarAllele" > $PREFIX.EPI.snp.final
    fi
  done

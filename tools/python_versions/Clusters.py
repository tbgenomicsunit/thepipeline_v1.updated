#! /usr/bin/env python3.7
# Copyrigth (C) 2022 Alvaro Chiner Oms
# adapted from Galo Adrian Goig's script

# This file is part of ThePipeline2
# ThePipeline2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# ThePipeline2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with ThePipeline2.  If not, see <http://www.gnu.org/licenses/>.

# Module for calculating the pairwise genetic distances (in number of SNPs)
# between eact pair of sequences in a FASTA file.

# This is script is thought to get as input a file of type
# "genetic_distances" with following structure
#
#
# Sample1   Distance  Sample2
# ERR1679588    803 G1986
# ERR1679585    2121    ERR1679588
# ERR1679587    2146    ERR1679588
# ERR1679586    2209    ERR1679588
#
# IMPORTANTLY, quotes MUST BE REMOVED in the input file


def ParseDistances(distfile, sep):

    with open(distfile) as infile:
        header = infile.readline()
        if sep == "Space":
            for line in infile:
                line = line.rstrip().split()
                yield line
        elif sep == "Tab":
            for line in infile:
                line = line.rstrip().split("\t")
                yield line


def getClusters(args):

    clusters = []
    for A, dist, B in ParseDistances(args.distfile, args.sep):
        clustered = False
        dist = int(dist)
        if dist <= args.threshold:
            if clusters:
                samples_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if A in cluster:
                        cluster.append(B)
                        samples_present_in.append(i)
                        clustered = True

                    elif B in cluster:
                        clustered = True
                        cluster.append(A)
                        samples_present_in.append(i)

                if not clustered:
                    newcluster = [A, B]
                    clusters.append(newcluster)

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in samples_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

            else:
                clusters.append([A, B])
        else:
            if clusters:
                A_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if A in cluster:
                        A_present_in.append(i)
                        clustered = True

                if not clustered:
                    clusters.append([A])

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in A_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

                clustered = False
                B_present_in = []
                for i in range(len(clusters)):
                    cluster = clusters[i]
                    if B in cluster:
                        B_present_in.append(i)
                        clustered = True

                if not clustered:
                    clusters.append([B])

                elif len(samples_present_in) > 1:
                    newcluster = []
                    offset = 0
                    for i in B_present_in:
                        newcluster.extend(clusters[i-offset])
                        del clusters[i-offset]
                        offset += 1
                    clusters.append(newcluster)

            else:
                clusters.append([A])
                clusters.append([B])

    return clusters


def deDupClusters(clusters):
    dedup = []
    for cluster in clusters:
        cluster = set(cluster)
        cluster = list(cluster)
        dedup.append(cluster)

    return dedup


def writeClusters(clusters, outfile):
    cont = 1
    file_out = open("{}.clusters.tsv".format(outfile), "w")
    file_out.write("Cluster\tSamples\n")
    for cluster in clusters:
        line = "Cluster_{}\t".format(cont)
        line += "{}".format(cluster[0])
        for sample in cluster[1:]:
            line += ",{}".format(sample)
        line += "\n"
        file_out.write(line)
        cont += 1
    file_out.close()


def writeClustersIrving(clusters, outfile):
    cont = 1
    file_out = open("{}.clusters.tsv".format(outfile), "w")
    file_out.write("Cluster\tSamples\n")
    for cluster in clusters:
        line = "Cluster_{}\t".format(cont)
        for sample in cluster:
            file_out.write("{}{}\n".format(line, sample))
        cont += 1
    file_out.close()


def GetClusters(args):
    import datetime

    if not args.outfile:
        e = datetime.datetime.now()
        args.outfile = e.strftime("%Y%m%d")

    clusters = getClusters(args)
    clusters = deDupClusters(clusters)
    if not args.output:
        writeClusters(clusters, args.outfile)
    else:
        writeClustersIrving(clusters, args.outfile)

    print("Calculated clusters written to"
          " {}.clusters.tsv file".format(args.outfile))

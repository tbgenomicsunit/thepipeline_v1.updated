#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

#To run 3_SNP_impact_prediction_v2.sh -i [path/to/snp_files] (accepts multiple -i) 
#reference datasets in koch /data2/reference_dataset
#Manejamos los argumentos de entrada
while getopts i: option;
do
        case "${option}"
        in
                i) INCLUDED_DATA+=("$OPTARG");;
                \?) exit 1;;
		:) exit 1;;
        esac
done

#la primer parte (comandos para generar los archivos .snp, .snp.indel y .snp.final) se hacen en el script anterior

#Script to extract the nonredundant list of SNP position HQ-homozygous calls, the script 1. extracts the nonredudant list; 2. annotates the positions; 3. removes the problematic regions (PE/PPE, phage/mobile, repeats) and generates the file for the next step: cluster.positions
echo "${green}Extracting the non-redundant positions...${reset}"

for path in "${INCLUDED_DATA[@]}" #search in all the folder for *.final and create nonredudant.positions.alleles from them
do
  echo "${path}"

   cat $path/*EPI.snp.final.annoF | grep -v Position >> positions_total #trabaja sobre los ficheros filtrados por anotacion

done

cat positions_total | sort -k2,2n -k4 | awk '{print $1, $2, ".", $3, $4, ".", ".", "."}' | sort -u -k2,2n  | sed 's/ /\t/g' > nonredudant.positions.alleles; 
echo "${green}Annotating the non-redundant positions...${reset}"
java -jar /data/ThePipeline_programs/snpEff/snpEff.jar -v -hgvs1LetterAa -noStats -no-downstream -no-intergenic -no-intron -no-upstream -noLof MTB_ancestor nonredudant.positions.alleles | grep -o '^[^#]*' | sed 's/|/\t/g' > result_snpeff.vcf

echo "${green}Creating the final SNP table...${reset}"
Rscript /data/ThePipeline/tools/lib/3_1_filter_snpeff_result_ThePipeline.R result_snpeff.vcf /data/Databases/MTB_annotation/H37Rv.annotation.tab /data/ThePipeline/tools/res/resistance_snps_list.csv SNP_table_final.txt


tail -n +2 SNP_table_final.txt | awk '{print $2, $3}' > cluster.positions

#This script must be run on a folder with all strains INITIAL callings (files: .snp, .snp.homo, .snp.het) The way the final list with recovered SNPs is built: 1. look in the homo file 0.95, 2. look in the hetero file 0.1 (at least 10% frequency and less than 95%), 3. wild type 
  
echo "${green}Creating multifasta file and final tables...${reset}"  
Rscript /data/ThePipeline/tools/lib/3_3_Create_multifasta_frequencies_ThePipeline.R cluster.positions "${INCLUDED_DATA[@]}" #crea un .fasta por cada .snp
rm run_alignment.fas run_alignment_gap.fas run_alignment_no_resis.fas

for path in "${INCLUDED_DATA[@]}" #search in all the folder for *.final and create nonredudant.positions.alleles from them
do
   cat $path/*.temp_ThePipeline.fasta >> run_alignment.fas;  #Y hace el multifasta
   sed -i 's/.*\//>/g' run_alignment.fas;
   rm $path/*.temp_ThePipeline.fasta;
   
done

#generamos los distintos multifastas

cat run_alignment.fas | sed '/^>/! s/[^ACGTacgt]/-/g' > run_alignment_gap.fas #elimina todos aquellos caracteres que no sean a c g t y los sustituye por - en las filas que no empiezan por >
res_pos=$(cat SNP_table_final.txt | awk -F '\t' '{if ($20=="TRUE") print NR -1}' | tr '\n' ',' | sed 's/.$//g') #extraemos las posiciones de resistencia

# Line added by Galo 14 June 2019.  If variable is empty because there is no resistance positions then just cp the file
if [ -z ${res_pos} ] 
  then
    cp run_alignment_gap.fas run_alignment_no_resis.fas
  else
    cat run_alignment_gap.fas | while read LINE
    do
      if [[ $LINE == \>* ]]
      then
        echo "$LINE" >> run_alignment_no_resis.fas #copiamos en run_alignment_no_resis.fas las lineas que comienzan por '>'
      else
        echo "$LINE" | cut -c "$res_pos" --complement >> run_alignment_no_resis.fas #copiamos las lineas que no comienzan por '>' pero eliminando aquellos caracteres que están en las posiciones de resistencia.
      fi
   done
fi

echo "${green}The alignment FILE run_aligment.fas has been created${reset}"

rm  *.vcf cluster.positions nonredudant.positions.alleles positions_total
echo "${green}Results printed to SNP_table_final.txt${reset}"


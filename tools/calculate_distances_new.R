#Script for calculating distances (in SNP number) between strains in order to identify transmission clusters.
# Use arguments is mandatory; 1: the multifasta file (with or without resistance), 2: the snp cutoff used

library(adegenet)
library(ape)
library(seqinr)
library(matrixcalc)
library(doParallel)
library(foreach)

args = commandArgs(trailingOnly=TRUE)
registerDoParallel(cores=12)

fasta <-fasta2DNAbin(file=args[1], snpOnly=FALSE)

dist=dist.dna(fasta, model="N",as.matrix=TRUE, pairwise.deletion = TRUE)
dist2=upper.triangle(as.matrix(dist))
total_distances=NULL
total_distances<-foreach(i=1:dim(dist2)[1], .errorhandling = 'remove') %dopar%
{
  distsample=NULL
  for (j in i:dim(dist2)[2]){
    if (rownames(dist2)[i]!=colnames(dist2)[j]) distsample=rbind(distsample,c(rownames(dist2)[i],dist2[i,j],colnames(dist2)[j]))
  }
  return(distsample)
}
distances=NULL
for(i in 1:length(total_distances))
{
  distances=rbind(distances, unlist(total_distances[[i]]))
}

name <- gsub(".fas","", args[1])

total_ordered=as.data.frame(distances[order(as.numeric(distances[,2])),])
if (dim(total_ordered)[2]==1) {total_ordered=t(total_ordered)}
total_ordered[,2]=as.numeric(as.character(total_ordered[,2]))
index=which(total_ordered[,2]<= as.integer(args[2]))
total_ordered_cutoff=total_ordered[index,]
write.table(total_ordered, file= paste("genetic_distances", name, sep = "_"), row.names = FALSE, quote = FALSE, col.names =c("Strain1", "diff", "Strain2"))
 if (dim(total_ordered)[1] > 1) {write.table(total_ordered_cutoff, file= paste("genetic_distances",name,args[2],"snp", sep="_"), row.names = FALSE, quote = FALSE, col.names =c("Strain1", "diff", "Strain2"))} else {write.table(t(total_ordered_cutoff), file= paste("genetic_distances",name,args[2],"snp", sep="_"), row.names = FALSE, quote = FALSE, col.names =c("Strain1", "diff", "Strain2"))}

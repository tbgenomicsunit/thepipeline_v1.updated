DATASET=Mozambique
PROCS=10
FIRSTFILE=$(ls *P1.clean.fastq.gz | cut -f1 -d"." | head -n 1)
REMAINFILES=$(ls *P1.clean.fastq.gz | cut -f1 -d"." | tail -n +2)

# First load kraken database with the first file
nice -n 5 ThePipeline kraken -f $FIRSTFILE.P1.clean.fastq.gz $FIRSTFILE.P2.clean.fastq.gz \
                   --paired --threads 20 --classify \
                   --filter --preload --prefix $FIRSTFILE --compressed --noclean 
# And then run kraken for all files. We are going to
# first run ThePip kraken --classify on each file and then
# ThePip kraken --filter in parallel with as many jobs
# as PROCS
for FILE in $REMAINFILES
  do
    nice -n 5 ThePipeline kraken -f $FILE.P1.clean.fastq.gz $FILE.P2.clean.fastq.gz --paired --threads 10 --classify --prefix $FILE --compressed --noclean
  done

# Now filter in parallel $REMAINFILES 
ls *P1.clean.fastq.gz | cut -f1 -d"." | tail -n +2 |
  xargs -P $PROCS -I {} nice -n 5 ThePipeline kraken -f {}.P1.clean.fastq.gz {}.P2.clean.fastq.gz --paired --filter --prefix {} --compressed --noclean

# Get kraken reports and genus/species contaminants
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
    while read prefix
      do
        kraken-report $prefix.kraken --db /data/Databases/KrakenDB/KRefSeqHuman/ > $prefix.report &&
        ThePipeline contaminants -p $prefix
      done

# Now map filtered reads with bwa. We are goin to use
# just 2 bwa threads as we are going to spawn PROCS
# number of ThePipeline mapping instances
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline mapping -f {}.P1.filtered.fastq {}.P2.filtered.fastq --prefix {} -t 2

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline coverage --prefix {}

# Now perform variant calling also in parallel
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline calling --prefix {}

# Filter SNPs by annotation, so we get rid of snps that fall in repeats/PPE
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
i   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline annotation_filter -s {}.DR.snp.final"

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline annotation_filter -s {}.EPI.snp.final"

# Filter SNPs near-deletions
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline del_filter -s {}DR*.annoF.snp -d {}.snp.del"

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline del_filter -s {}EPI*.annoF.snp -d {}.snp.del"


# Now build the SNP tables for all the run in both, DR and EPI branches
ThePipeline mergesnp -s *.DR.annofilter.snp.final > DATASET.DR.mergesnp
ThePipeline mergesnp -s *.EPI.annofilter.snp.final > DATASET.EPI.mergesnp

# Now rescue snp according to mergesnp tables
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline rescue -m DATASET.DR.mergesnp --rescue-from {}.snp --rescue-to {}.DR.annofilter.snp.final --min-perc-strains 0.1 --min-cov 10 -p {} > {}.DR.annofilter.rescued.snp.final"

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 sh -c "ThePipeline rescue -m DATASET.EPI.mergesnp --rescue-from {}.snp --rescue-to {}.EPI.annofilter.snp.final --min-perc-strains 0.1 --min-cov 20 -p {} > {}.EPI.annofilter.rescued.snp.final"

# Calc resistance and lineages
ThePipeline resistance -s *annoF.delF.snp

# Calc genetic distances
ThePipeline distance
mv genetic_distances DATASET.Kraken.gendistances

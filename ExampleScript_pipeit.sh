#! /bin/bash
# This script is supposed to start from trimmomatic filtered files
# through ThePipeline clean ending in P1/2.clean.fastq.gz

PROCS=6
FIRSTFILE=$(ls *P1.clean.fastq.gz | cut -f1 -d"." | head -n 1)
REMAINFILES=$(ls *P1.clean.fastq.gz | cut -f1 -d"." | tail -n +2)

# First load kraken database with the first file
nice -n 5 ThePipeline kraken -f $FIRSTFILE.P1.clean.fastq.gz $FIRSTFILE.P2.clean.fastq.gz \
                   --paired --threads 20 --classify \
                   --filter --preload --prefix $FIRSTFILE --compressed
# And then run kraken for all files. We are going to
# first run ThePip kraken --classify on each file and then
# ThePip kraken --filter in parallel with as many jobs
# as PROCS
for FILE in $REMAINFILES
  do
    nice -n 5 ThePipeline kraken -f $FILE.P1.clean.fastq.gz $FILE.P2.clean.fastq.gz --paired --threads 10 --classify --prefix $FILE --compressed
  done

# Now filter in parallel $REMAINFILES 
ls *P1.clean.fastq.gz | cut -f1 -d"." | tail -n +2 |
  xargs -P $PROCS -I {} nice -n 5 ThePipeline kraken -f {}.P1.clean.fastq.gz {}.P2.clean.fastq.gz --paired --filter --prefix {} --compressed

# Now map filtered reads with bwa. We are goin to use
# just 2 bwa threads as we are going to spawn PROCS
# number of ThePipeline mapping instances

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline mapping -f {}\_1.filtered.fastq {}\_2.filtered.fastq --prefix {} -t 2

ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline coverage --prefix {}

# Now perform variant calling also in parallel
ls *P1.clean.fastq.gz | cut -d '.' -f1 |
   xargs -P $PROCS -I {} nice -n 5 ThePipeline calling --prefix {}

# And finally predict antibiotic resistances
ThePipeline resistance

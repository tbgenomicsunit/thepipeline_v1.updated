#! /usr/bin/env python

def parse_args():
    '''Parse arguments given to script'''

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--report", "-r", required=True, dest="report")

    parser.add_argument("--study", "-s", required=True, dest="study",
        help=("Dataset name to append to each row to identify the dataframe"))
    
    args = parser.parse_args()

    return args 

def ReportParser(report_file):
    report_dic = {}
    with open(report_file) as infile:
        header = infile.readline().rstrip()
        for line in infile:
            line = line.rstrip().split("\t")
            sample  = line[0]
            values = line[1:]
            report_dic[sample] = values

    return report_dic, header

def ReformatDict(report_dic, study):
    
    for sample in report_dic:
        values = report_dic[sample]
        dirty_values = []
        clean_values = []
        for value in values:
            value = value.split(":")
            if len(value) == 1:
                dirty_values.append(value[0])
                clean_values.append(value[0])
            elif len(value) == 2:
                dirty_values.append(value[0])
                clean_values.append(value[1])
            else:
                assert False
        dirty_values.append("Dirty")
        dirty_values.append(study)
        clean_values.append("Clean")
        clean_values.append(study)
        report_dic[sample] = (dirty_values, clean_values)

    return report_dic

def WriteDict(report_dic, header):
    import sys

    header += "\tDataset\tStudy\n"
    sys.stdout.write(header)
    for sample in report_dic:
        dirty_values, clean_values = report_dic[sample]
        clean_line = sample
        for cvalue in clean_values:
            clean_line += "\t"
            clean_line += cvalue
        clean_line += "\n"
        dirty_line = sample
        for dvalue in dirty_values:
            dirty_line += "\t"
            dirty_line += dvalue
        dirty_line += "\n"
        sys.stdout.write(dirty_line)
        sys.stdout.write(clean_line)

    return 0

def main():

    args = parse_args()
    report_dic, header = ReportParser(args.report)
    report_dic = ReformatDict(report_dic, args.study)
    WriteDict(report_dic, header)

if __name__ == "__main__":
    main()